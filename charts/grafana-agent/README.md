# Grafana Agent Helm chart

* Installs the [grafana agent](https://grafana.com/docs/agent)

## Configuration

| Key                                   | Type   | Default           | Description                                                                                     |
|---------------------------------------|--------|-------------------|-------------------------------------------------------------------------------------------------|
| affinity                              | object | `{}`              | Affinity policies applied to DaemonSet                                                          |
| config.extraArgs                      | list   | `[]`              | Extra args used by grafana-agent                                                                |
| config.integrations                   | object | `{}`              | Integrations section from the grafana-agent config                                              |
| config.logs                           | object | `{}`              | Logs sction from the grafana-agent config                                                       |
| config.metrics                        | object | `{}`              | Metrics section from the grafana-agent config                                                   |
| config.traces                         | object | `{}`              | Traces section from the grafana-agent config                                                    |
| config.server.logLevel                | string | `"info"`          | Log only messages with the given severity or above. Supported values [debug, info, warn, error] |
| extraVolumeMounts                     | list   | `[]`              | Extra volumeMounts to use in the grafana-agent container                                        |
| extraVolumes                          | list   | `[]`              | Extra volumes to use in the grafana-agent daemonSet                                             |
| fullnameOverride                      | string | `""`              | Full name overrie for release                                                                   |
| image.pullPolicy                      | string | `"IfNotPresent"`  | Image pull policy                                                                               |
| image.repository                      | string | `"grafana/agent"` | Grafana Agent image                                                                             |
| image.tag                             | string | `""`              | Grafana Agent tag. If not defined, use Chart.appVersion                                         |
| imagePullSecrets                      | list   | `[]`              | Pull secret for private repository                                                              |
| nameOverride                          | string | `""`              | Release name override                                                                           |
| nodeSelector                          | object | `{}`              | Pod node selector                                                                               |
| podAnnotations                        | object | `{}`              | Grafana Agent pod annotations                                                                   |
| podSecurityContext                    | object | `{}`              | Pod Security Context                                                                            |
| resources                             | object | `{}`              | Kubernetes pod resources                                                                        |
| securityContext                       | object | `{}`              | Container securityContext                                                                       |
| service.port                          | int    | `80`              | Kubernetes service port                                                                         |
| service.type                          | string | `"ClusterIP"`     | Service Type                                                                                    |
| serviceAccount.annotations            | object | `{}`              | ServiceAccount annotations                                                                      |
| serviceAccount.create                 | bool   | `true`            | Specifies whether a ServiceAccount should be created                                            |
| serviceAccount.name                   | string | `""`              | The name of the ServiceAccount to create                                                        |
| tolerations                           | list   | `[]`              | Kubernetes tolerations applied to DaemonSet                                                     |

