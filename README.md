# POC Helm deployment tools

## Pre requisites

- [vault CLI](https://www.vaultproject.io/downloads) - v1.10.0
- [helm](https://helm.sh/docs/intro/install/) - 3
- [helm-secrets](https://github.com/jkroepke/helm-secrets/wiki/Installation) - 3.8.4
- [helmfile](https://github.com/roboll/helmfile#installation) - v0.143.1
- [docker](https://docs.docker.com/engine/install/) - 20.10.6
- [kind](https://kind.sigs.k8s.io/docs/user/quick-start/) - v0.11.1
- [dasel](https://daseldocs.tomwright.me/installation) - v1.24.0
- [direnv](https://direnv.net/#basic-installation) (optional) - 2.28.0


## Steps

#### 1. Configure the enviornment

##### 1.1. Manually

```bash
mkdir -p $PWD/.kube
export KUBECONFIG=$PWD/.kube/config
export VAULT_ADDR=VAULT_URL
export VAULT_TOKEN=VAULT_TOKEN
```

##### 1.2. Direnv (optional)

- Check the `.envrc` file
- Allow it
    ```bash
    direnv allow
    ```

#### 2. Create kind clusters

```bash
kind create cluster
```

#### 3. Configure sops with Vault

```bash
./bin/prepare_local_vault
```

## TODO

- [x] Multiple cluster
- [x] Multiple environment
- [x] Use Vault for secrets

