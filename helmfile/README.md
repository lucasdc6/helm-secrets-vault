# helmfile POC

```bash
helmfile -e virginia -l cluster=apps apply
```

## Get enviornmnets

```bash
dasel -f helmfile.yaml -m ".environments.[*].kubeContext"
```

## Get clusters

```bash
dasel -f helmfile.yaml -m ".releases.[*].labels.cluster"
```
